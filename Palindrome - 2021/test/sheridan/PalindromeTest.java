package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		boolean isPalindrome = Palindrome.isPalindrome("racecar");
		assertTrue("Unable to Validate Palindrome", isPalindrome);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean isPalindrome = Palindrome.isPalindrome("james");
		assertFalse("Unable to Validate Palindrome", isPalindrome);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean isPalindrome = Palindrome.isPalindrome("race car");
		assertTrue("Unable to Validate Palindrome", isPalindrome);
	}
	
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean isPalindrome = Palindrome.isPalindrome("james samejj");
		assertFalse("Unable to Validate Palindrome", isPalindrome);
	}	
	
}
